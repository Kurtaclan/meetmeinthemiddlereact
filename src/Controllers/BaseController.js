import { Router } from 'express';
const yelp = require('yelp-fusion');

const client = yelp.client('kdVPHaYB0bAYiKz9GiZuf5b4o8f76heRz-wkCsWJDAHyvHxfCNCZPKiaE0BRG5WcgqBNQv6y2y-vvpJRkxXQVMmf9L5l47IFxXGEIUsS6Rz0mLvXayxPjONVasLwWnYx');
const _router = Router();

_router.get('/SearchResults/:searchTerm/:categories/:price/:latitude/:longitude', async function (req, res, next) {
    let search = {
        term: req.params.searchTerm,
        latitude: req.params.latitude, // 37.786882
        longitude: req.params.longitude, // -122.399972
        radius: 805,
        limit: 20,
        open_now: true
    };

    if (req.params.price !== 'test') {
        search.price = req.params.price;
    }
    if (req.params.categories !== 'test') {
        search.categories = req.params.categories;
    }

    return client.search(search).then(response => res.status(200).send(response.jsonBody.businesses)
    ).catch((error) => res.status(400).send('error'));
});

export const router = _router;
export const path = '/api/Yelp';

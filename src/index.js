import express from 'express';
import path from 'path';

// helmet, cors, and jwttoken are plugins for security purposes
import helmet from 'helmet';
import cors from 'cors';

// Express cannot parse the body of requests by default, this adds that functionality
import bodyParser from 'body-parser';
import { router, path as routePath } from './Controllers/BaseController';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

global.appRoot = path.resolve(__dirname);

const app = express();

// Cross origin settings
const corsOptions = {
    origin: process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : 'http://juristerr-react.s3-website.us-east-2.amazonaws.com/',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

// Setting up the filters to improve security and stability
app.use(cors(corsOptions));
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Controllers/Routers
app.use(routePath, router);

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

app.get('*', (req, res) => {
    res.sendFile(path.join(path.join(__dirname, '/client/build/index.html')));
});

// Response to errors to prevent failed requests from travelling upwards
if (process.env.NODE_ENV === 'development') {
    app.use(function (err, req, res, next) {
        console.log(err);
    });
} else {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500 || 400).json({
            status: 'error',
            message: 'An error has occurred'
        });
    });
}

// port of the service
const port = process.env.PORT || 5000;

// start the app
app.listen(port);
console.log('Listening on port: ' + port);

module.exports = app;

import { combineReducers } from 'redux';
import dataReducer from './Reducers/DataReducer';

const appReducer = combineReducers({
    dataReducer
});

export default appReducer;
import * as actionType from '../ActionTypes';

export function SetLocation(location) {
    return {
        type: actionType.SET_LOCATION,
        payload: location
    };
}

export function SetSearch(search) {
    return {
        type: actionType.SET_SEARCH,
        payload: search
    };
}

import * as actionType from '../ActionTypes';
import { getCenter } from 'geolib';

let initialState = {
    location: { latitude: 0, longitude: 0 },
    locations: [],
    search: {
        rating: [],
        price: [],
        searchTerm: '',
        categories: []
    }
};

const dataReducer = (state = initialState, action) => {
    let newState = {
        location: { latitude: 0, longitude: 0 },
        locations: [],
        search: {
            rating: [],
            price: [],
            searchTerm: '',
            categories: []
        }
    };

    switch (action.type) {
        case actionType.SET_LOCATION:
            newState.location = getCenter(action.payload.map(location => { return { latitude: location.lat, longitude: location.long } }))
            newState.locations = action.payload;
            newState.search = state.search;

            return newState;
        case actionType.SET_SEARCH:
            newState.search = action.payload;
            newState.locations = state.locations;
            newState.location = state.location;

            return newState;
        default:
            return state;
    }
};

export default dataReducer;

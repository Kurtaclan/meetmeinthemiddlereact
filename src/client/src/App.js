import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import ScrollToTop from './Components/Routing/ScrollToTop';
import Home from './Components/Views/Home';
import NavMenu from './Components/Views/NavMenu';
import FAQ from './Components/Views/FAQ';
import Privacy from './Components/Views/Privacy';
import Footer from './Components/Views/Footer';
import * as MeetMe from './Components/Views/MeetMe';

class App extends Component {
    render() {
        return (
            <Router>
                <ScrollToTop>
                    <div>
                        <Route
                            path="/"
                            component={NavMenu} />
                        <Switch>
                            <Route exact path="/" component={Home} />
                            <Route path="/FAQ" component={FAQ} />
                            <Route path="/Privacy" component={Privacy} />
                            <Route path="/MeetMe/Start" component={MeetMe.Start} />
                            <Route path="/MeetMe/Search" component={MeetMe.Search} />
                            <Route path="/MeetMe/Result" component={MeetMe.Result} />
                            <Route path="/MeetMe/Send" component={MeetMe.Send} />
                            <Route path="/MeetMe/Next" component={MeetMe.Next} />
                            <Route path="/MeetMe/Prev" component={MeetMe.Prev} />
                        </Switch>
                        <Route
                            path="/"
                            component={Footer} />
                    </div>
                </ScrollToTop>
            </Router>
        );
    }
}

export default App;

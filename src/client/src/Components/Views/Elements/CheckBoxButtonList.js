import React, { Component } from 'react';
import { InputGroup } from "react-bootstrap";

export default class CheckBoxButtonList extends Component {
    constructor(props, context) {
        super(props, context);
        this.btnClick = this.btnClick.bind(this);

        var buttons = [];

        for (var i = this.props.valueMin; i <= this.props.valueMax; i++) {
            buttons.push({
                checked: false,
                value: i
            });
        }

        this.state = {
            buttons: buttons
        };
    }

    btnClick(i) {
        var buttons = this.state.buttons;
        buttons[i].checked = !buttons[i].checked

        var result = this.state.buttons.filter((value) => value.checked).map((button) => {
            return button.value;
        })

        this.props.updateParentState(this.props.checkBoxClass, result);

        this.setState({ buttons: buttons });
    }

    icons(number) {
        var icons = [];

        for (var i = 0; i < number; i++) {
            icons.push(<i key={i + 'icon'} className={`fa ${this.props.faIcon}`}></i>);
        }

        return icons;
    }

    list() {
        return this.state.buttons.map((button, i) => {
            return (
                <div key={i + 'ck'} className={`ck-button ${this.state.buttons[i].checked ? "checked" : ""}`}>
                    <label>
                        <input type="checkbox" onClick={() => this.btnClick(i)} className={this.props.checkBoxClass} value={this.props.value} />
                        <span>{this.icons(i + 1)}</span>
                    </label>
                </div>
            );
        });
    }

    render() {
        return (
            <InputGroup bsSize="large">
                {this.list()}
            </InputGroup>
        );
    }
}
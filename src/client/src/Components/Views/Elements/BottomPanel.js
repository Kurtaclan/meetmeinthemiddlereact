import React, { Component } from 'react';
import { LinkContainer } from 'react-router-bootstrap';
import { Row, Col, Panel, Button } from 'react-bootstrap';

export default class BottomPanel extends Component {
    panel() {
        return (
            <Panel>
                <Panel.Body>
                    <Col xs={6}>
                        <label className="stepField">Step {this.props.step} of 4</label>
                    </Col>
                    <Col xs={6}>
                        <div className="pull-right">
                            {
                                this.props.location.pathname !== '/MeetMe/Start' ?
                                    <LinkContainer to={{ pathname: '/MeetMe/Prev', state: { curPath: this.props.location.pathname } }}>
                                        <Button className="previous">Previous</Button>
                                    </LinkContainer> : ""
                            }
                            <LinkContainer to={{ pathname: '/MeetMe/Next', state: { curPath: this.props.location.pathname } }}>
                                <Button onClick={this.props.setAppState} disabled={!this.props.canSubmit} bsStyle="primary" className="continue">Next</Button>
                            </LinkContainer>
                        </div>
                    </Col>
                </Panel.Body>
            </Panel>);
    }

    render() {
        return (
            <div>
                <Row className="hidden-xs">
                    {this.panel()}
                </Row>
                <div className="footer navbar-fixed-bottom visible-xs">
                    {this.panel()}
                </div>
            </div>
        );
    }
}

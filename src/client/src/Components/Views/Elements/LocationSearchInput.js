import React, { Component } from 'react';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

export default class LocationSearchInput extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.reset = this.reset.bind(this);

        this.state = { tempAddress: this.props.location.address, location: this.props.location };
    }

    handleChange = (address) => {
        this.setState({ tempAddress: address });
    }

    onBlur() {
        if (this.state.location.address !== this.state.tempAddress) {
            this.reset();
        }
    }

    reset() {
        this.props.updateParentState({ address: "", lat: 0, long: 0 }, this.props.index);
        this.setState({ tempAddress: "" });
    }

    onSelect() {
        this.reset();
    }

    handleSelect = (address) => {
        geocodeByAddress(address)
            .then(results => getLatLng(results[0]))
            .then(latLng => {
                this.props.updateParentState({ address: address, lat: latLng.lat, long: latLng.lng }, this.props.index);
                this.setState({ location: { address: address, lat: latLng.lat, long: latLng.lng } });
            })
            .catch(error => console.error('Error', error))
    }

    render() {
        return (
            <PlacesAutocomplete
                value={this.state.tempAddress}
                onChange={this.handleChange}
                onSelect={this.handleSelect}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                    <div>
                        <div className="input-group input-group-lg">
                            <span className="input-group-addon"><span className="fa fa-map-marker"></span></span>
                            <input
                                {...getInputProps({
                                    placeholder: 'Search Places ...',
                                    className: 'location-search-input form-control'
                                })}
                                onBlur={this.onBlur}
                                onClick={this.onSelect}
                                id={this.props.id}
                            />
                        </div>
                        {suggestions.length > 0 && this.state.tempAddress !== "" ?
                            <div className="autocomplete-dropdown-container">
                                {suggestions.map(suggestion => {
                                    const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                    return (
                                        <div {...getSuggestionItemProps(suggestion, { className, style })}>
                                            <span>{suggestion.description}</span>
                                        </div>
                                    )
                                })}
                            </div> : ""
                        }
                    </div>
                )}
            </PlacesAutocomplete>
        );
    }
}
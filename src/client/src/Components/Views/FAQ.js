import React, { Component } from 'react';
import { Row, Col, Panel } from 'react-bootstrap';
import { Link } from "react-router-dom";


export default class FAQ extends Component {
    render() {
        return (
            <div className="container">
                <Row>
                    <Panel>
                        <Row>
                            <Panel>
                                <Panel.Heading>
                                    <Col xs={12}>
                                        <h4>Frequently Asked Questions</h4>
                                    </Col>
                                </Panel.Heading>
                            </Panel>
                        </Row>

                        <Row className="row">
                            <Panel>
                                <Panel.Heading>
                                    <Col xs={12}>
                                        <h4>What is Meet Me In The Middle?</h4>
                                    </Col>
                                </Panel.Heading>
                                <Panel.Body>
                                    <Col xs={12}>
                                        Meet Me In The Middle is a tool to help idividuals to find a meeting point in between their various locations.  Be it food, coffee, or just a place to have a conversation, our tool is there to help.
                                    </Col>
                                </Panel.Body>
                            </Panel>
                        </Row>
                        <Row>
                            <Panel>
                                <Panel.Heading>
                                    <Col xs={12}>
                                        <h4>Privacy Policy</h4>
                                    </Col>
                                </Panel.Heading>
                                <Panel.Body>
                                    <Col xs={12}>
                                        We promise not to sell, distribute, or make available any personal infromation collected via our site. (e.g. Your Location, Email, and/or Phone Numbers.)
                                        <br />
                                        For a full statement please view our <Link to="/Privacy">Privacy Policy</Link>.
                                    </Col>
                                </Panel.Body>
                            </Panel>
                        </Row>

                        <Row>
                            <Panel>
                                <Panel.Heading>
                                    <Col xs={12}>
                                        <h4>How Can I Help?</h4>
                                    </Col>
                                </Panel.Heading>
                                <Panel.Body>
                                    <Col xs={12}>
                                        While we test and improve Meet Me In The Middle you can contact us at <a href="mailto:meetmeinthemiddleteam@gmail.com?subject=Feedback And Questions">meetmeinthemiddleteam@gmail.com</a>
                                    </Col>
                                </Panel.Body>
                            </Panel>
                        </Row>

                        <Row>
                            <Panel>
                                <Panel.Heading>
                                    <Col xs={12}>
                                        <h4>Donate</h4>
                                    </Col>
                                </Panel.Heading>
                                <Panel.Body>
                                    <Col xs={12}>
                                        Want to help us keep the lights on? Send us a buck with Paypal:
                                <br />
                                        <br />
                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                            <input type="hidden" name="cmd" value="_s-xclick"></input>
                                            <input type="hidden" name="hosted_button_id" value="W6FLWHR2AK988"></input>
                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"></input>
                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"></img>
                                        </form>
                                    </Col>
                                </Panel.Body>
                            </Panel>
                        </Row>
                    </Panel>
                </Row>
            </div>);
    }
}
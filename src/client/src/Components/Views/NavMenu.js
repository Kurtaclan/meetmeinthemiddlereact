import { NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { MenuItem, Navbar, Nav } from 'react-bootstrap';
import React, { Component } from 'react';

export const NavLink = ({ to, eventKey, children }) =>
    <LinkContainer to={to} eventKey={eventKey}>
        <NavItem>{children}</NavItem>
    </LinkContainer>;

export const IndexNavLink = ({ to, eventKey, children }) =>
    <LinkContainer exact to={to} eventKey={eventKey}>
        <NavItem>{children}</NavItem>
    </LinkContainer>;

export const MenuLink = ({ to, eventKey, children }) =>
    <LinkContainer to={to} eventKey={eventKey}>
        <MenuItem>{children}</MenuItem>
    </LinkContainer>;

export default class NavMenu extends Component {
    render() {
        return (
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <NavLink to="/">
                            <img className="profile-header-img" src="/Middle_Icon.png" alt="Juristerra" />
                        </NavLink>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav pullRight>
                        <IndexNavLink eventKey={2} to='/' >
                            Home
                        </IndexNavLink>
                        <NavLink eventKey={3} to='/FAQ' >
                            FAQ
                        </NavLink>
                        <NavLink eventKey={4} to='/Privacy' >
                            Privacy
                        </NavLink>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>);
    }
}
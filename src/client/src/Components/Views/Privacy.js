import React, { Component } from 'react';
import { Row, Col, Panel, ListGroup, ListGroupItem } from 'react-bootstrap';
import { HashLink } from 'react-router-hash-link';

export default class Privacy extends Component {
    render() {
        return (
            <div className="container">
                <Row>
                    <Panel>
                        <Row>
                            <Panel>
                                <Panel.Heading>
                                    <Col xs={12}>
                                        <h4>Privacy Policy</h4>
                                    </Col>
                                </Panel.Heading>
                            </Panel>
                        </Row>

                        <Row>
                            <Panel>
                                <ListGroup>
                                    <ListGroupItem>
                                        <strong>
                                            <HashLink to="#collection">Information Collection</HashLink>
                                        </strong>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <strong>
                                            <HashLink to="#usage">Information Usage</HashLink>
                                        </strong>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <strong>
                                            <HashLink to="#cookie">Cookie Usage</HashLink>
                                        </strong>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <strong>
                                            <HashLink to="#disclosure">3rd Party Disclosure</HashLink>
                                        </strong>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <strong>
                                            <HashLink to="#links">3rd Party Links</HashLink>
                                        </strong>
                                    </ListGroupItem>
                                </ListGroup>
                            </Panel>
                        </Row>
                        <Row>
                            <Panel>
                                <Panel.Body>
                                    <Col xs={12}>
                                        This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information'
                                        (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own
                                        or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our
                                        privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable
                                        Information in accordance with our website.
                                    </Col>
                                </Panel.Body>
                            </Panel>
                        </Row>

                        <Row id='collection'>
                            <Panel>
                                <Panel.Heading>
                                    <Col xs={12}>
                                        <strong>What personal information do we collect from the people that visit our blog, website or app?</strong>
                                    </Col>
                                </Panel.Heading>
                                <Panel.Body>
                                    <Col xs={12}>
                                        When using or registering on our site, as appropriate, you may be asked to enter your name, email address,
                                        phone number, Personal Address or other details to help you with your experience.
                                    </Col>
                                </Panel.Body>
                            </Panel>
                        </Row>

                        <Row>
                            <Panel>
                                <Panel.Heading>
                                    <Col xs={12}>
                                        <strong>When do we collect information?</strong>
                                    </Col>
                                </Panel.Heading>
                                <Panel.Body>
                                    <Col xs={12}>
                                        We collect information from you when you register on our site, or fill out a form on our site.
                                    </Col>
                                </Panel.Body>
                            </Panel>
                        </Row>

                        <Row id='usage'>
                            <Panel>
                                <Panel.Heading>
                                    <Col xs={12}>
                                        <strong>How do we use your information?</strong>
                                    </Col>
                                </Panel.Heading>
                                <Panel.Body>
                                    <Col xs={12}>
                                        We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey
                                        or marketing communication, surf the website, or use certain other site features in the following ways:
                                        <br />
                                        <br />
                                        <strong>&bull;</strong> To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.
                                        <br />
                                        <strong>&bull;</strong> To improve our website in order to better serve you.
                                    </Col>
                                </Panel.Body>
                            </Panel>
                        </Row>

                        <Row id='cookie'>
                            <Panel>
                                <Panel.Heading>
                                    <Col xs={12}>
                                        <strong>Do we use 'cookies'?</strong>
                                    </Col>
                                </Panel.Heading>
                                <Panel.Body>
                                    <Col xs={12}>
                                        Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser
                                        (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For
                                        instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences
                                        based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate
                                        data about site traffic and site interaction so that we can offer better site experiences and tools in the future.
                                    </Col>
                                </Panel.Body>
                                <Panel.Body>
                                    <strong>We use cookies to:</strong>
                                    <br />
                                    <strong>&bull;</strong> Understand and save user's preferences for future visits.
                                </Panel.Body>
                                <Panel.Body>
                                    You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser
                                    settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.
                                    <br />
                                    If you turn cookies off, some features will be disabled. It won't affect the user's experience that make your site experience more efficient and may not function
                                    properly.
                                    <br />
                                    However, you will still be able to use the service.
                                </Panel.Body>
                            </Panel>
                        </Row>

                        <Row className='panel panel-default' id="disclosure">
                            <Panel>
                                <Panel.Heading>
                                    <strong>Third-party disclosure</strong>
                                </Panel.Heading>
                                <Panel.Body>
                                    We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information.
                                </Panel.Body>
                            </Panel>
                        </Row>
                        <Row className='panel panel-default' id="links">
                            <Panel>
                                <Panel.Heading>
                                    <strong>Third-party links</strong>
                                </Panel.Heading>
                                <Panel.Body>
                                    Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy
                                    policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site
                                    and welcome any feedback about these sites.
                                </Panel.Body>
                            </Panel>
                        </Row>
                        <Row className='panel panel-default'>
                            <Panel>
                                <Panel.Heading>
                                    <strong>Google</strong>
                                </Panel.Heading>
                                <Panel.Body>
                                    Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users.
                                    https://support.google.com/adwordspolicy/answer/1316548?hl=en
                                    <br />
                                    We have not enabled Google AdSense on our site but we may do so in the future.
                                </Panel.Body>
                            </Panel>
                        </Row>
                        <Row>
                            <Panel>
                                <Panel.Heading>
                                    <strong>California Online Privacy Protection Act</strong>
                                </Panel.Heading>
                                <Panel.Body>
                                    CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy.  The law's reach stretches well
                                    beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable
                                    Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals
                                    or companies with whom it is being shared. -  See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf
                                </Panel.Body>
                                <Panel.Body>
                                    <strong>According to CalOPPA, we agree to the following:</strong><br />
                                    <br />
                                    Users can visit our site anonymously.
                                    <br />
                                    Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website.
                                    <br />
                                    Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.
                                </Panel.Body>
                                <Panel.Body>
                                    You will be notified of any Privacy Policy changes:
                                    <br />
                                    <strong>&bull;</strong> On our Privacy Policy Page<br />
                                </Panel.Body>
                                <Panel.Body>
                                    Can change your personal information:
                                    <br />
                                    <strong>&bull;</strong> By logging in to your account
                                </Panel.Body>
                                <Panel.Body>
                                    <strong>How does our site handle Do Not Track signals?</strong>
                                    <br />
                                    We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.
                                </Panel.Body>
                                <Panel.Body>
                                    <strong>Does our site allow third-party behavioral tracking?</strong><br />
                                    <br />
                                    It's also important to note that we do not allow third-party behavioral tracking
                                </Panel.Body>
                            </Panel>
                        </Row>
                        <Row>
                            <Panel>
                                <Panel.Heading>
                                    <strong>COPPA (Children Online Privacy Protection Act)</strong>
                                </Panel.Heading>
                                <Panel.Body>
                                    When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control.  The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.
                                </Panel.Body>
                                <Panel.Body>
                                    We do not specifically market to children under the age of 13 years old.
                                </Panel.Body>
                            </Panel>
                        </Row>
                        <Row>
                            <Panel>
                                <Panel.Heading>
                                    <strong>Fair Information Practices</strong>
                                </Panel.Heading>
                                <Panel.Body>
                                    The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role
                                    in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical
                                    to comply with the various privacy laws that protect personal information.
                                </Panel.Body>
                                <Panel.Body>
                                    <strong>In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</strong>
                                    <br />
                                    We will notify you via email
                                    <br />
                                    <strong>&bull;</strong> Within 10 business days
                                </Panel.Body>
                                <Panel.Body>
                                    We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors
                                    and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals
                                    have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.
                                </Panel.Body>
                            </Panel>
                        </Row>
                        <Row>
                            <Panel>
                                <Panel.Heading>
                                    <strong>CAN SPAM Act</strong>
                                </Panel.Heading>
                                <Panel.Body>
                                    The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped
                                    from being sent to them, and spells out tough penalties for violations.
                                </Panel.Body>
                                <Panel.Body>
                                    <strong>We collect your email address in order to:</strong>
                                    <br />
                                    <strong>&bull;</strong> Send information, respond to inquiries, and/or other requests or questions
                                </Panel.Body>
                                <Panel.Body>
                                    <strong>To be in accordance with CANSPAM, we agree to the following:</strong>
                                    <br />
                                    <strong>&bull;</strong> Not use false or misleading subjects or email addresses.
                                </Panel.Body>
                                <Panel.Body>
                                    <strong>&bull;</strong> Identify the message as an advertisement in some reasonable way.
                                    <br />
                                    <strong>&bull;</strong> Include the physical address of our business or site headquarters.
                                    <br />
                                    <strong>&bull;</strong> Monitor third-party email marketing services for compliance, if one is used.
                                    <br />
                                    <strong>&bull;</strong> Honor opt-out/unsubscribe requests quickly.
                                </Panel.Body>
                            </Panel>
                        </Row>
                        <Row>
                            <Panel>
                                <Panel.Heading>
                                    <strong>Contacting Us</strong>
                                </Panel.Heading>
                                <Panel.Body>
                                    If there are any questions regarding this privacy policy, you may contact us using the information below.
                                    <br />
                                    meetmeinthemiddleteam@gmail.com
                                </Panel.Body>
                            </Panel>
                        </Row>
                        <Row>
                            <Panel>
                                <Panel.Body>
                                    Last Edited on 4-30-2018
                                </Panel.Body>
                            </Panel>
                        </Row>
                    </Panel>
                </Row>
            </div>);
    }
}
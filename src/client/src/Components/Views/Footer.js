import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Footer extends Component {
    render() {
        return (
            <div className="container footer">
                <hr className="hidden-sm hidden-xs" />
                <footer className="hidden-xs hidden-sm">
                    <span className="pull-left">&copy; {(new Date()).getFullYear()} - Meet Me In The Middle Team</span>
                    <span className="pull-right"><Link to="/Privacy">Privacy Policy</Link></span>
                </footer>
            </div>);
    }
}
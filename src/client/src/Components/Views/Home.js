import React, { Component } from 'react';
import { LinkContainer } from 'react-router-bootstrap';
import { Row, Col, Panel, Button } from 'react-bootstrap';

export default class Home extends Component {
    render() {
        return (
            <div className="container">
                <Row>
                    <Col>
                        <Panel>
                            <Panel.Heading>
                                <h1>Meet Me in the Middle</h1>
                            </Panel.Heading>
                            <Panel.Body>
                                <p>
                                    Meet Me in the Middle is an app to help People who need to meet up but can't decide where.
                                    <br />
                                    It triangualtes everyones location to find food, shopping, or entertainment in the exact geographic middle.
                                </p>
                            </Panel.Body>
                            <Panel.Footer>
                                <LinkContainer exact to="/MeetMe/Start">
                                    <Button bsStyle="primary" bsSize="large" block >Get Started</Button>
                                </LinkContainer>
                            </Panel.Footer>
                        </Panel>
                    </Col>
                </Row>
            </div>);
    }
}
import React, { Component } from 'react';
import { Row, Col, Panel, InputGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';
import { toWordsOrdinal } from 'number-to-words';
import ReactLoading from 'react-loading';
import LocationSearchInput from './Elements/LocationSearchInput';
import BottomPanel from './Elements/BottomPanel';
import CheckBoxButtonList from "./Elements/CheckBoxButtonList";
import * as Actions from "../../Redux/Actions/DataActions";

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function addressHasValue(value) {
    return value.address !== ""
}

class next extends Component {
    redirectNext() {
        if (this.props.location.state.curPath === "/MeetMe/Start") {
            return (<Redirect to="/MeetMe/Search" />);
        }
        else if (this.props.location.state.curPath === "/MeetMe/Search") {
            return (<Redirect to="/MeetMe/Result" />);
        }
        else if (this.props.location.state.curPath === "/MeetMe/Result") {
            return (<Redirect to="/MeetMe/Send" />);
        }
        else {
            return (<Redirect to="/" />);
        }
    }

    render() {
        return (
            <div>
                {this.redirectNext()}
            </div>
        );
    }
}

class prev extends Component {
    redirectNext() {
        if (this.props.location.state.curPath === "/MeetMe/Search") {
            return (<Redirect to="/MeetMe/Start" />);
        }
        else if (this.props.location.state.curPath === "/MeetMe/Result") {
            return (<Redirect to="/MeetMe/Search" />);
        }
        else if (this.props.location.state.curPath === "/MeetMe/Send") {
            return (<Redirect to="/MeetMe/Result" />);
        }
        else {
            return (<Redirect to="/" />);
        }
    }

    render() {
        return (
            <div>
                {this.redirectNext()}
            </div>
        );
    }
}

class start extends Component {
    constructor(props, context) {
        super(props, context);
        this.Locations = this.Locations.bind(this);
        this.updatePeople = this.updatePeople.bind(this);
        this.updateParentState = this.updateParentState.bind(this);
        this.setAppState = this.setAppState.bind(this);

        var menuItems = [];

        for (var i = 2; i <= 8; i++) {
            menuItems.push(<option key={i + "option"}>{i}</option>);
        }

        this.state = {
            canSubmit: this.props.data.locations.length !== 0 && this.props.data.locations.every(addressHasValue),
            locations: this.props.data.locations,
            menuItems: menuItems,
            people: this.props.data.locations.length
        };
    }

    updateParentState(location, index) {
        var locations = this.state.locations;
        locations[index] = location;
        var canSubmit = false;

        if (locations.every(addressHasValue)) {
            canSubmit = true
        }


        this.setState({
            locations: locations,
            canSubmit: canSubmit
        })
    }

    updatePeople(event) {
        var people = parseInt(event.target.value, 10);

        if (this.state.locations.length !== people) {
            let locations = this.state.locations.slice(0, Math.min(this.state.locations.length, people));
            var canSubmit = false;

            for (var i = this.state.locations.length; i < people; i++) {
                locations.push({
                    address: "",
                    lat: "",
                    long: ""
                });
            }

            if (locations.every(addressHasValue)) {
                canSubmit = true
            }

            this.setState({ locations: locations, people: people, canSubmit: canSubmit });
        }
    }

    setAppState() {
        this.props.dispatch(Actions.SetLocation(this.state.locations));
    }

    Locations() {
        return this.state.locations.map(function (location, i) {
            if (i % 2 === 0) {
                return (
                    <Row key={i + "location"}>
                        <Panel>
                            <Panel.Body>
                                <Col sm={6}>
                                    <Row>
                                        <Col sm={8}>
                                            <span className="form-span titlecase">Enter {i === 0 ? "Your" : `${capitalizeFirstLetter(toWordsOrdinal(i))} Person's`} Address</span>
                                            {(i === 0) ?
                                                <div className="visible-sm textLine">
                                                </div> : ""
                                            }
                                        </Col>
                                    </Row>
                                    <LocationSearchInput location={this.state.locations[i]} updateParentState={this.updateParentState} index={i} id={`Person${i}`} />
                                </Col>
                                {this.state.locations.length > i + 1 ?
                                    <Col sm={6}>
                                        <Row>
                                            <Col sm={8}>
                                                <span className="form-span titlecase">{`Enter ${capitalizeFirstLetter(toWordsOrdinal(i + 1))} Person's Address`}</span>
                                            </Col>
                                        </Row>
                                        <LocationSearchInput location={this.state.locations[i + 1]} updateParentState={this.updateParentState} index={i + 1} id={`Person${i + 1}`} />
                                    </Col> : ""
                                }
                            </Panel.Body>
                        </Panel>
                    </Row>
                );
            }
            else {
                return "";
            }
        }, this);
    }

    render() {
        return (
            <div className="container">
                <Row>
                    <Panel>
                        <Row>
                            <Panel>
                                <Panel.Body>
                                    <Row>
                                        <Col xs={12}>
                                            <InputGroup bsSize="large">
                                                <span className="form-span">Select the Number of People</span>
                                                <select id="people_amount" value={this.state.people} onChange={this.updatePeople} className="form-control" placeholder="Choose the Amount of People">
                                                    <option>--</option>
                                                    {this.state.menuItems}
                                                </select>
                                            </InputGroup>
                                        </Col>
                                    </Row>
                                </Panel.Body>
                            </Panel>
                        </Row>

                        {this.Locations()}

                        <div className="visible-xs bottom-spacer">
                        </div>

                        <BottomPanel location={this.props.location} step={1} canSubmit={this.state.canSubmit} setAppState={this.setAppState} />
                    </Panel>
                </Row>
            </div>);
    }
}

class search extends Component {
    constructor(props, context) {
        super(props, context);

        this.onChange = this.onChange.bind(this);
        this.checkBoxChange = this.checkBoxChange.bind(this);
        this.updateParentState = this.updateParentState.bind(this);
        this.setAppState = this.setAppState.bind(this);

        this.state = {
            rating: this.props.data.search.rating,
            price: this.props.data.search.price,
            searchTerm: this.props.data.search.searchTerm,
            categories: this.props.data.search.categories
        };
    }

    setAppState() {
        this.props.dispatch(Actions.SetSearch(this.state));
    }

    onChange(event) {
        this.setState({
            searchTerm: event.target.value
        })
    }


    checkBoxChange(event) {
        var value = event.target.value;
        var array = this.state.categories;
        var index = array.indexOf(value);

        if (index !== -1) {
            array.splice(index, 1);
        }
        else {
            array.push(value);
        }

        this.setState({
            categories: array
        })
    }

    updateParentState(type, values) {
        switch (type) {
            case 'Rating':
                this.setState({
                    rating: values,
                });
                break;
            case 'Price':
                this.setState({
                    price: values,
                });
                break;
            default:
                break;
        }
    }

    render() {
        if (this.props.data.locations.length === 0) {
            return (<Redirect to='/MeetMe/Start' />)
        }
        else {
            return (
                <div className="container">
                    <Row>
                        <Panel>
                            <Row>
                                <Panel>
                                    <Panel.Body>
                                        <Col xs={12}>
                                            <span className="form-span">Enter Search Terms</span>
                                            <InputGroup bsSize="large">
                                                <InputGroup.Addon><span className="fa fa-search"></span></InputGroup.Addon>
                                                <input value={this.state.searchTerm} onChange={this.onChange} className="form-control" />
                                            </InputGroup>
                                        </Col>
                                    </Panel.Body>
                                </Panel>
                            </Row>
                            <Row>
                                <Panel>
                                    <Panel.Body>
                                        <Col md={6}>
                                            <span className="form-span">Choose Price Range</span>
                                            <CheckBoxButtonList updateParentState={this.updateParentState} checkBoxClass="Price" valueMin={1} valueMax={4} faIcon="fa-usd" />
                                        </Col>
                                    </Panel.Body>
                                </Panel>
                            </Row>
                            <Row>
                                <Panel>
                                    <Panel.Body>
                                        <Col xs={12}>
                                            <span className="form-span">Choose Categories</span>
                                            <InputGroup bsSize="large">
                                                <label>
                                                    <input onChange={this.checkBoxChange} className="Categories" type="checkbox" value="vegetarian" />
                                                    <span>Vegetarian</span>
                                                </label>
                                                <label>
                                                    <input onChange={this.checkBoxChange} className="Categories" type="checkbox" value="burgers" />
                                                    <span>Burgers</span>
                                                </label>
                                                <label>
                                                    <input onChange={this.checkBoxChange} className="Categories" type="checkbox" value="sandwiches" />
                                                    <span>Sandwiches</span>
                                                </label>
                                                <label>
                                                    <input onChange={this.checkBoxChange} className="Categories" type="checkbox" value="mexican" />
                                                    <span>Mexican</span>
                                                </label>
                                                <label>
                                                    <input onChange={this.checkBoxChange} className="Categories" type="checkbox" value="hotdogs" />
                                                    <span>Fast Food</span>
                                                </label>
                                                <label>
                                                    <input onChange={this.checkBoxChange} className="Categories" type="checkbox" value="pizza" />
                                                    <span>Pizza</span>
                                                </label>
                                                <label>
                                                    <input onChange={this.checkBoxChange} className="Categories" type="checkbox" value="coffee" />
                                                    <span>Coffee</span>
                                                </label>
                                                <label>
                                                    <input onChange={this.checkBoxChange} className="Categories" type="checkbox" value="gluten_free" />
                                                    <span>Gluten Free</span>
                                                </label>
                                            </InputGroup>
                                        </Col>
                                    </Panel.Body>
                                </Panel>
                            </Row>

                            <div className="visible-xs bottom-spacer">
                            </div>

                            <BottomPanel location={this.props.location} step={2} canSubmit={this.state.searchTerm !== ""} setAppState={this.setAppState} />
                        </Panel>
                    </Row>
                </div>
            );
        }
    }
}

class result extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            selectedResult: this.props.data.selectedResult,
            searchResults: null
        };
    }

    async componentWillMount() {
        await fetch(`/api/Yelp/SearchResults/${this.props.data.search.searchTerm}/${this.props.data.search.categories.length === 0 ? 'test' : this.props.data.search.categories.join(',')}/${this.props.data.search.price.length === 0 ? 'test' : this.props.data.search.price.join(',')}/${this.props.data.location.latitude}/${this.props.data.location.longitude}`, {
            method: 'GET',
        })
            .then(response => response.json())
            .then((response) => {
                if (this.state.searchResults !== response) {
                    this.setState({ searchResults: response });
                }
            }).catch((ex) => {
                console.log(ex);
            });
    }

    searchRating(result) {
        if (result.rating && result.rating !== 0) {
            var ratings = [];
            for (var i = 1; i < result.rating; i++) {
                ratings.push(<span key={i} className="fa fa-star"></span>);
            }

            if (!(result.rating % 1 === 0)) {
                ratings.push(<span key={0} className="fa fa-star-half"></span>);
            }

            return (
                <a target="_blank" href={`https://www.yelp.com/biz/${result.id}`}>
                    {ratings}
                </a>
            );
        }
        else {
            return (
                <a target="_blank" href={`https://www.yelp.com/biz/${result.id}`}>
                    Rating Unavailable
                </a>
            );
        }
    }

    searchPrice(result) {
        if (result.price) {
            var prices = [];
            for (var i = 1; i < result.price.length; i++) {
                prices.push(<span key={i} className="fa fa-dollar"></span>);
            }

            return prices;
        }
        else {
            return (
                <span>Price Unavailable</span>
            );
        }
    }

    getAddress(location) {
        let address = [location.address1, location.address2, location.address3].join(' ').trim();
        return `${address}, ${location.city}, ${location.zip_code}`;
    }

    searchResult(result) {
        let address = this.getAddress(result.location);
        return (
            <div className={`row panel panel-default yelp-result ${this.state.selectedResult === result ? 'selected' : ''}`} onClick={() => this.setState({ selectedResult: result })} data-location={address}>
                <div className="col-xs-12">
                    <div className="row panel-body panel-bottomborder relative-parent">
                        <div className="absolute-top-left">
                            {result.name}
                        </div>
                        <div className="absolute-bottom-left">
                            {
                                address !== "" ?
                                    <a target="_blank" href={`https://www.google.com/maps/place/${address.replace(" ", "+")}`}>{address}</a>
                                    : <span>Address Unavailable</span>
                            }
                        </div>

                        <div className="col-xs-9">
                        </div>
                        <div className="col-xs-3 yelp-icon-container">
                            <img className="yelp-icon" src={result.image_url === "" ? "https://s3-media3.fl.yelpcdn.com/assets/srv0/yelp_styleguide/fe8c0c8725d3/assets/img/default_avatars/business_90_square.png" : result.image_url} alt={result.name} />
                        </div>
                    </div>
                    <div className="row panel-body spacer panel-bottomborder">
                    </div>
                    <div className="row panel-body panel-bottomborder">
                        <div className="col-xs-6">
                            {this.searchRating(result)}
                        </div>
                        <div className="col-xs-6">
                            {
                                result.display_phone ?
                                    <a className="full-right" target="_blank" href={`tel:${result.phone}`}>{result.display_phone}</a>
                                    : <span className="full-right">Phone Number Unavailable</span>
                            }
                        </div>
                    </div>
                </div>
            </div>);
    }

    render() {
        return (
            this.props.data.locations.length === 0 || this.props.data.search.searchTerm === '' ?
                <Redirect to='/MeetMe/Start' />
                : !this.state.searchResults ?
                    <ReactLoading className='center' type='balls' color='#000000' height='200px' width='200px' />
                    : <div className='container'>
                        <Row>
                            <Panel>
                                {
                                    this.state.searchResults.map((result) => { return this.searchResult(result) })
                                }
                                <div className="visible-xs bottom-spacer">
                                </div>

                                <BottomPanel location={this.props.location} step={3} canSubmit={this.state.selectedResult !== null} setAppState={this.setAppState} />
                            </Panel>
                        </Row>
                    </div>
        );
    }
}

class send extends Component {
    render() {
        return (
            <div className='container'>
                <Row>
                    <Panel>
                        <Row>
                            <Panel>
                                <Panel.Body>
                                    <Col xs={12}>
                                        <span className="form-span">Enter Search Terms</span>
                                        <InputGroup bsSize="large">
                                            <InputGroup.Addon><span className="fa fa-search"></span></InputGroup.Addon>
                                            <input value={this.state.SearchTerm} onChange={this.onChange} className="form-control" />
                                        </InputGroup>
                                    </Col>
                                </Panel.Body>
                            </Panel>
                        </Row>

                        <div className="visible-xs bottom-spacer">
                        </div>

                        <BottomPanel location={this.props.location} step={2} canSubmit={this.state.searchTerm !== ""} setAppState={this.setAppState} />

                    </Panel>
                </Row>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        data: state.dataReducer
    };
}

export const Start = connect(mapStateToProps)(withRouter(start));
export const Search = connect(mapStateToProps)(withRouter(search));
export const Result = connect(mapStateToProps)(withRouter(result));
export const Send = connect(mapStateToProps)(withRouter(send));
export const Next = withRouter(next);
export const Prev = withRouter(prev);